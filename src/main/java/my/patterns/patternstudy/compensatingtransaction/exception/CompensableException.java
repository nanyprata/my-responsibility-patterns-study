package my.patterns.patternstudy.compensatingtransaction.exception;

public class CompensableException extends Exception {

	private static final long serialVersionUID = 2235960992227001131L;

	public CompensableException(String message) {
		super(message);
	}
}
