package my.patterns.patternstudy.compensatingtransaction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import my.patterns.patternstudy.compensatingtransaction.exception.CompensableException;

public class IterativeTask {

    private List<IterativeStep> steps;

    public IterativeTask(IterativeStep... steps) {
        this.steps = Arrays.asList(steps);
    }

    public void doSomething() {
        List<Compensation> compensationList = new ArrayList<>();

        try {
            for (IterativeStep step: steps) {
                step.perform();
                compensationList.add(step.getCompensation());
            }
        } catch (CompensableException e) {
            compensationList.forEach(Compensation::compensate);
        }
    }
}
