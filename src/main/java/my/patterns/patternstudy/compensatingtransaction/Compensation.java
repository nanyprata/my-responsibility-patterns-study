package my.patterns.patternstudy.compensatingtransaction;

public interface Compensation {
    void compensate();
}
