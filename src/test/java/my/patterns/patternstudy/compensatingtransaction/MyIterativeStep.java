package my.patterns.patternstudy.compensatingtransaction;

import my.patterns.patternstudy.compensatingtransaction.Compensation;
import my.patterns.patternstudy.compensatingtransaction.Step;
import my.patterns.patternstudy.compensatingtransaction.exception.CompensableException;

public class MyIterativeStep extends Step {

    private boolean success = true;

	private boolean perform = false;

	public MyIterativeStep(Compensation compensation) {
        super(compensation);
    }

	@Override
	public void perform() throws CompensableException {
		if (!success) {
			throw new CompensableException("Exception on step N");
		}
		super.perform();
		perform = true;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public boolean isPerform() {
		return perform;
	}

	public void setPerform(boolean perform) {
		this.perform = perform;
	}
}
