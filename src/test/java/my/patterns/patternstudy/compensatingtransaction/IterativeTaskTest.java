package my.patterns.patternstudy.compensatingtransaction;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import my.patterns.patternstudy.compensatingtransaction.IterativeTask;

public class IterativeTaskTest {

	@Test
	public void test_doSomething_success() {

	    MyCompensation compensationOne = new MyCompensation();
	    MyIterativeStep one = new MyIterativeStep(compensationOne);
		one.setSuccess(true);

		MyCompensation compensationTwo = new MyCompensation();
		MyIterativeStep two = new MyIterativeStep(compensationTwo);
		two.setSuccess(true);

		MyCompensation compensationThree = new MyCompensation();
		MyIterativeStep three = new MyIterativeStep(compensationThree);
		three.setSuccess(true);

		IterativeTask task = new IterativeTask(one, two, three);
		task.doSomething();

		assertTrue(one.isPerform());
		assertTrue(two.isPerform());
		assertTrue(three.isPerform());

		assertFalse(((MyCompensation) one.getCompensation()).isCompensate());
		assertFalse(((MyCompensation) two.getCompensation()).isCompensate());
		assertFalse(((MyCompensation) three.getCompensation()).isCompensate());
	}

	@Test
	public void test_doSomething_stepOneFailure() {

        MyCompensation compensationOne = new MyCompensation();
        MyIterativeStep one = new MyIterativeStep(compensationOne);
        one.setSuccess(false);

        MyCompensation compensationTwo = new MyCompensation();
        MyIterativeStep two = new MyIterativeStep(compensationTwo);
        two.setSuccess(true);

        MyCompensation compensationThree = new MyCompensation();
        MyIterativeStep three = new MyIterativeStep(compensationThree);
        three.setSuccess(true);

        IterativeTask task = new IterativeTask(one, two, three);
        task.doSomething();

		assertFalse(one.isPerform());
		assertFalse(two.isPerform());
		assertFalse(three.isPerform());

        assertFalse(((MyCompensation) one.getCompensation()).isCompensate());
        assertFalse(((MyCompensation) two.getCompensation()).isCompensate());
        assertFalse(((MyCompensation) three.getCompensation()).isCompensate());
	}

	@Test
	public void test_doSomething_stepTwoFailure() {

        MyCompensation compensationOne = new MyCompensation();
        MyIterativeStep one = new MyIterativeStep(compensationOne);
        one.setSuccess(true);

        MyCompensation compensationTwo = new MyCompensation();
        MyIterativeStep two = new MyIterativeStep(compensationTwo);
        two.setSuccess(false);

        MyCompensation compensationThree = new MyCompensation();
        MyIterativeStep three = new MyIterativeStep(compensationThree);
        three.setSuccess(true);

        IterativeTask task = new IterativeTask(one, two, three);
        task.doSomething();

		assertTrue(one.isPerform());
		assertFalse(two.isPerform());
		assertFalse(three.isPerform());

        assertTrue(((MyCompensation) one.getCompensation()).isCompensate());
        assertFalse(((MyCompensation) two.getCompensation()).isCompensate());
        assertFalse(((MyCompensation) three.getCompensation()).isCompensate());
	}

	@Test
	public void test_doSomething_stepThreeFailure() {
        MyCompensation compensationOne = new MyCompensation();
        MyIterativeStep one = new MyIterativeStep(compensationOne);
        one.setSuccess(true);

        MyCompensation compensationTwo = new MyCompensation();
        MyIterativeStep two = new MyIterativeStep(compensationTwo);
        two.setSuccess(true);

        MyCompensation compensationThree = new MyCompensation();
        MyIterativeStep three = new MyIterativeStep(compensationThree);
        three.setSuccess(false);

        IterativeTask task = new IterativeTask(one, two, three);
        task.doSomething();

		assertTrue(one.isPerform());
		assertTrue(two.isPerform());
		assertFalse(three.isPerform());

        assertTrue(((MyCompensation) one.getCompensation()).isCompensate());
        assertTrue(((MyCompensation) two.getCompensation()).isCompensate());
        assertFalse(((MyCompensation) three.getCompensation()).isCompensate());
	}

	@Test
	public void test_doSomething_stepFourFailure() {
        MyCompensation compensationOne = new MyCompensation();
        MyIterativeStep one = new MyIterativeStep(compensationOne);
        one.setSuccess(true);

        MyCompensation compensationTwo = new MyCompensation();
        MyIterativeStep two = new MyIterativeStep(compensationTwo);
        two.setSuccess(true);

        MyCompensation compensationThree = new MyCompensation();
        MyIterativeStep three = new MyIterativeStep(compensationThree);
        three.setSuccess(true);

        MyCompensation compensationFour = new MyCompensation();
        MyIterativeStep four = new MyIterativeStep(compensationFour);
        four.setSuccess(false);

        IterativeTask task = new IterativeTask(one, two, three, four);
        task.doSomething();

		assertTrue(one.isPerform());
		assertTrue(two.isPerform());
		assertTrue(three.isPerform());
		assertFalse(four.isPerform());

        assertTrue(((MyCompensation) one.getCompensation()).isCompensate());
        assertTrue(((MyCompensation) two.getCompensation()).isCompensate());
        assertTrue(((MyCompensation) three.getCompensation()).isCompensate());
        assertFalse(((MyCompensation) four.getCompensation()).isCompensate());
	}

}