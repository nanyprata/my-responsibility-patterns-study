package my.patterns.patternstudy.compensatingtransaction;

import my.patterns.patternstudy.compensatingtransaction.Compensation;

public class MyCompensation implements Compensation{

    private boolean compensate = false;

    @Override
    public void compensate() {
        this.compensate = true;
    }

    public boolean isCompensate() {
        return compensate;
    }
}
